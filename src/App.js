import React from 'react';
import check from './svgs/icon-check-16.svg';
import cross from './svgs/icon-cross-16.svg';
import crossWhite from './svgs/icon-cross-16-white.svg';
import arrowDown from './svgs/icon-arrow-down-16.svg';
import thinker from './svgs/thinker-v5.svg';
import './App.css';
import check8white from './svgs/check-8-white.png';
import check16white from './svgs/check-16-white.png';
import x8white from './svgs/x-8-white.png';
import x16white from './svgs/x-16-white.png';
import x8dark from './svgs/x-8-dark.png';
import x16dark from './svgs/x-16-dark.png';
import check8dark from './svgs/check-8-dark.png';
import check16dark from './svgs/check-16-dark.png';
import xBlueCircleHollow from './svgs/x-blue-circle-hollow.png';
import xBlueCircleFilled from './svgs/x-blue-circle-filled.png';
import checkBlueCircleHollow from './svgs/check-blue-circle-hollow.png';
import checkBlueCircleFilled from './svgs/check-blue-circle-filled.png';
import lineHollow from './svgs/line-hollow.png';
import lineFilled from './svgs/line-filled.png';
import x8blue from './svgs/x-8-blue.png';
import x8grey from './svgs/x-8-grey.png';
import check8blue from './svgs/check-8-blue.png';
import check8grey from './svgs/check-8-grey.png';
import hkGroteskLight from './otherfiles/hk-grotesk/HKGrotesk-Light.otf';
import layer1bg from './svgs/layer-1-bg.svg';
import layer1content from './svgs/layer-1-content.svg';
import layer2bg from './svgs/layer-2-bg.svg';
import layer2content from './svgs/layer-2-content.svg';
import layer3bg from './svgs/layer-3-bg.svg';
import layer3content from './svgs/layer-3-content.svg';

import mountain1 from './svgs/mountain1.svg';
import mountain2 from './svgs/mountain2.svg';
import mountain3 from './svgs/mountain3.svg';
import mountain4 from './svgs/mountain4.svg';
import mountain5 from './svgs/mountain5.svg';

function App() {
  return (
    <div className="App">
      <img src={mountain1} />
      <img src={mountain2} />
      <img src={mountain3} />
      <img src={mountain4} />
      <img src={mountain5} />
      <img src={check} />
      <img src={cross} />
      <img src={crossWhite} />
      <img src={arrowDown} />
      <img src={thinker} />
      <img src={check8white} />
      <img src={check16white} />
      <img src={x8white} />
      <img src={x16white} />
      <img src={check8dark} />
      <img src={check16dark} />
      <img src={x8dark} />
      <img src={x16dark} />
      <img src={xBlueCircleHollow} />
      <img src={xBlueCircleFilled} />
      <img src={checkBlueCircleHollow} />
      <img src={checkBlueCircleFilled} />
      <img src={lineHollow} />
      <img src={lineFilled} />
      <img src={check8blue} />
      <img src={check8grey} />
      <img src={x8blue} />
      <img src={x8grey} />
      <img src={layer1bg} />
      <img src={layer1content} />
      <img src={layer2bg} />
      <img src={layer2content} />
      <img src={layer3bg} />
      <img src={layer3content} />
      <a href={hkGroteskLight}>hkGroteskLight</a>
    </div>
  );
}

export default App;
